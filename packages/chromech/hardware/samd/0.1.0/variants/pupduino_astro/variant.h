/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _VARIANT_ARDUINO_ZERO_
#define _VARIANT_ARDUINO_ZERO_

// The definitions here needs a SAMD core >=1.6.10
#define ARDUINO_SAMD_VARIANT_COMPLIANCE 10610

/*----------------------------------------------------------------------------
 *        Definitions
 *----------------------------------------------------------------------------*/

/** Frequency of the board main oscillator */
#define VARIANT_MAINOSC		(32768ul)

/** Master clock frequency */
#define VARIANT_MCK			  (48000000ul)

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "WVariant.h"

#ifdef __cplusplus
#include "SERCOM.h"
#include "Uart.h"
#endif // __cplusplus

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*----------------------------------------------------------------------------
 *        Pins
 *----------------------------------------------------------------------------*/

// Number of pins defined in PinDescription array
#define PINS_COUNT           (36u)
#define NUM_DIGITAL_PINS     (36u)
#define NUM_ANALOG_INPUTS    (0u)
#define NUM_ANALOG_OUTPUTS   (0u)
#define analogInputToDigitalPin(p)  ((p < 6u) ? (p) + 14u : -1)

#define digitalPinToPort(P)        ( &(PORT->Group[g_APinDescription[P].ulPort]) )
#define digitalPinToBitMask(P)     ( 1 << g_APinDescription[P].ulPin )
//#define analogInPinToBit(P)        ( )
#define portOutputRegister(port)   ( &(port->OUT.reg) )
#define portInputRegister(port)    ( &(port->IN.reg) )
#define portModeRegister(port)     ( &(port->DIR.reg) )
#define digitalPinHasPWM(P)        ( g_APinDescription[P].ulPWMChannel != NOT_ON_PWM || g_APinDescription[P].ulTCChannel != NOT_ON_TIMER )

/*
 * digitalPinToTimer(..) is AVR-specific and is not defined for SAMD
 * architecture. If you need to check if a pin supports PWM you must
 * use digitalPinHasPWM(..).
 *
 * https://github.com/arduino/Arduino/issues/1833
 */
// #define digitalPinToTimer(P)

/*
 * LEDs
 */
#define PIN_RGBLED_RED		 (0u)
#define PIN_RGBLED_GRN       (1u)
#define PIN_RGBLED_BLU		 (2u)

#define PIN_LED_RXL          (4u)
#define PIN_LED_TXL          (3u)

#define PIN_LED_13           PIN_LED_TXL
#define PIN_LED              PIN_LED_TXL
#define PIN_LED2             PIN_LED_RXL
#define PIN_LED3             PIN_LED_RXL
#define LED_BUILTIN          PIN_LED_TXL

// Map between schematic name and friendly spelling.
#define PIN_RGBLED_GREEN PIN_RGBLED_GRN
#define PIN_RGBLED_BLUE PIN_RGBLED_BLU

#define ADC_RESOLUTION		12

/*
 * Serial interfaces
 */
// Serial1 (SWO)
#define PIN_SERIAL1_TX       (35ul)
#define PAD_SERIAL1_TX       (UART_TX_PAD_0)
#define PIN_SERIAL1_RX       (0ul) // Unconnected
#define PAD_SERIAL1_RX       (SERCOM_RX_PAD_1) // Avoid using Pad 0, which is TX
#define PERIPH_SWO_SERIAL    sercom5

static const uint8_t PIN_SWO_TX = PIN_SERIAL1_TX ;

/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 1 // Shared between WiFi and OLED

// Wi-Fi
#define PIN_WIFI_SPI_MISO         (5u)
#define PIN_WIFI_SPI_MOSI         (6u)
#define PIN_WIFI_SPI_SCK          (7u)
#define PIN_WIFI_SPI_CS		      (8u)
#define PERIPH_WIFI_SPI           sercom4
#define PAD_WIFI_SPI_TX           SPI_PAD_2_SCK_3
#define PAD_WIFI_SPI_RX           SERCOM_RX_PAD_0

static const uint8_t WIFI_SS   = PIN_WIFI_SPI_CS ;
static const uint8_t WIFI_MOSI = PIN_WIFI_SPI_MOSI ;
static const uint8_t WIFI_MISO = PIN_WIFI_SPI_MISO ;
static const uint8_t WIFI_SCK  = PIN_WIFI_SPI_SCK ;

#define PIN_WIFI_SPI_IRQ          (9u)
#define PIN_WIFI_RESET_CPU        (10u)
#define PIN_WIFI_CHIP_EN          (11u)
#define PIN_WIFI_WAKE             (12u)

// OLED
#define PIN_OLED_SPI_MISO		  PIN_WIFI_SPI_MISO
#define PIN_OLED_SPI_MOSI         (26u)
#define PIN_OLED_SPI_SCK          (27u)
#define PIN_OLED_SPI_CS		      (28u)
#define PERIPH_OLED_SPI           sercom4
#define PAD_OLED_SPI_TX           SPI_PAD_0_SCK_1
#define PAD_OLED_SPI_RX	          SERCOM_RX_PAD_2

static const uint8_t OLED_SS   = PIN_OLED_SPI_CS ;
static const uint8_t OLED_MOSI = PIN_OLED_SPI_MOSI ;
static const uint8_t OLED_SCK  = PIN_OLED_SPI_SCK ;

#define PIN_OLED_DC               (29u)
#define PIN_OLED_RST              (30u)

/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 2

// Onboard TC Reader
#define PIN_TC_WIRE_SDA           (13u)
#define PIN_TC_WIRE_SCL           (14u)
#define PERIPH_TC_WIRE            sercom1
#define TC_WIRE_IT_HANDLER        SERCOM1_Handler

static const uint8_t TC_SDA = PIN_TC_WIRE_SDA;
static const uint8_t TC_SCL = PIN_TC_WIRE_SCL;

#define PIN_TC_ALERT1             (15u)
#define PIN_TC_ALERT2             (16u)
#define PIN_TC_ALERT3             (17u)
#define PIN_TC_ALERT4             (18u)

// Application Sensors Board
#define PIN_APP_WIRE_SDA          (19u)
#define PIN_APP_WIRE_SCL          (20u)
#define PERIPH_APP_WIRE           sercom0
#define APP_WIRE_IT_HANDLER       SERCOM0_Handler

static const uint8_t APP_SDA = PIN_APP_WIRE_SDA;
static const uint8_t APP_SCL = PIN_APP_WIRE_SCL;

#define PIN_APP_SENSORS_GPIO1     (21u)
#define PIN_APP_SENSORS_GPIO2     (22u)
#define PIN_APP_SENSORS_GPIO3     (23u)
#define PIN_APP_SENSORS_GPIO4     (24u)
  
/*
 * USB
 */
#define PIN_USB_HOST_ENABLE PIO_NOT_A_PIN
#define PIN_USB_DM          (38ul)
#define PIN_USB_DP          (39ul)

/*
 * GPIO
 */
#define PIN_SSR_FIRE              (25u)

#define PIN_UI_ENC_A              (31u)
#define PIN_UI_ENC_B              (32u)
#define PIN_UI_PUSH               (33u)
#define PIN_BUZZER_SWITCH         (34u)

/*
 * DEFAULTS
 * Some libraries assume you have a default SPI, Wire, etc. device defined.
 * Map that here.
 */
// Serial
#define Serial SerialUSB

// SPI
#ifdef ASTRO_HAS_WIFI
  #define PIN_SPI_MISO PIN_WIFI_SPI_MISO
  #define PIN_SPI_MOSI PIN_WIFI_SPI_MOSI
  #define PIN_SPI_SCK  PIN_WIFI_SPI_SCK
  #define PIN_SPI_CS   PIN_WIFI_SPI_CS
  #define PAD_SPI_TX   PAD_WIFI_SPI_TX
  #define PAD_SPI_RX   PAD_WIFI_SPI_RX
  #define PERIPH_SPI   PERIPH_WIFI_SPI
#else
  #define PIN_SPI_MISO PIN_OLED_SPI_MISO
  #define PIN_SPI_MOSI PIN_OLED_SPI_MOSI
  #define PIN_SPI_SCK  PIN_OLED_SPI_SCK
  #define PIN_SPI_CS   PIN_OLED_SPI_CS	
  #define PAD_SPI_TX   PAD_OLED_SPI_TX
  #define PAD_SPI_RX   PAD_OLED_SPI_RX
  #define PERIPH_SPI   PERIPH_OLED_SPI
#endif

#define WIFI_SERCOM_PIN_MODE PIO_SERCOM_ALT
#define OLED_SERCOM_PIN_MODE PIO_SERCOM_ALT

// Wire
#define PIN_WIRE_SDA PIN_APP_WIRE_SDA
#define PIN_WIRE_SCL PIN_APP_WIRE_SCL
#define PERIPH_WIRE PERIPH_APP_WIRE

#define PIN_WIRE1_SDA PIN_TC_WIRE_SDA
#define PIN_WIRE1_SCL PIN_TC_WIRE_SCL
#define PERIPH_WIRE1 PERIPH_TC_WIRE

/*
 * HACK: wiring_analog.c somewhat sloppily assumes that there's at least one analog pin
 *       labeled A0 and PIN_A0 on the board.  This isn't the case on Astro, so this hack
 *       keeps their code from falling apart.
 */
 #define A0                       (0u)   // For analogRead.  If you call analogRead with no analog pins,
                                         // you're gonna have a bad time.
 #define PIN_A0                   PIO_NOT_A_PIN // In analogWrite, forces a return if you try to write to a DAC.

#ifdef __cplusplus
}
#endif

/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus

/*	=========================
 *	===== SERCOM DEFINITION
 *	=========================
*/
extern SERCOM sercom0;
extern SERCOM sercom1;
extern SERCOM sercom2;
extern SERCOM sercom3;
extern SERCOM sercom4;
extern SERCOM sercom5;

extern Uart Serial5;

#endif

// These serial port names are intended to allow libraries and architecture-neutral
// sketches to automatically default to the correct port name for a particular type
// of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
// the first hardware serial port whose RX/TX pins are not dedicated to another use.
//
// SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
//
// SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
//
// SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
//
// SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
//
// SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
//                            pins are NOT connected to anything by default.
#define SERIAL_PORT_USBVIRTUAL      SerialUSB
#define SERIAL_PORT_MONITOR         SerialUSB
// Serial has no physical pins broken out, so it's not listed as HARDWARE port
#define SERIAL_PORT_HARDWARE        Serial1
#define SERIAL_PORT_HARDWARE_OPEN   Serial1

#endif /* _VARIANT_ARDUINO_ZERO_ */

