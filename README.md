# README #

This is a public repository for Chromech's Arduino-compatible boards.

### What is this repository for? ###

* As of right now, it'll mostly hold the board files for Arduino to import.

### How do I get set up? ###

* You don't, you probably don't need this.

### Contribution guidelines ###

* Be Sean.  Otherwise, if you feel inclined to send a pull request for a board you can't buy, have at it.

### Who do I talk to? ###

* The hand, 'cuz the face is somewhat preoccupied with documentation.